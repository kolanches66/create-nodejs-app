## create-node-js

Boilerplate for basic cli nodejs app

### How to create project via Git:
```
git clone git@bitbucket.org:kolanches66/create-nodejs-app.git test-node \
  && cd test-node \
  && rm -rf .git \
  && rm -f README.md \ 
  && yarn
```
