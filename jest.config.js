module.exports = {
  verbose: false,
  testEnvironment: "node",
  testPathIgnorePatterns: ["<rootDir>/test/api"],
  testRegex: "(/test/.*\\.test\\.js)$",
  transform: {
    "^.+\\.js$": "babel-jest"
  }
};
